
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.2")

addSbtPlugin("com.sksamuel.scapegoat" % "sbt-scapegoat" % "1.0.9")

addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.2.1")

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

object Formatting {

  lazy val settings = Seq(
    ScalariformKeys.preferences := ScalariformKeys.preferences.value
      .setPreference(DanglingCloseParenthesis, Force)
      .setPreference(DoubleIndentConstructorArguments, false)
      .setPreference(DoubleIndentMethodDeclaration, true)
      .setPreference(FormatXml, false)
      .setPreference(IndentLocalDefs, true)
      .setPreference(IndentPackageBlocks, true)
      .setPreference(MultilineScaladocCommentsStartOnFirstLine, true)
      .setPreference(NewlineAtEndOfFile, true)
      .setPreference(PlaceScaladocAsterisksBeneathSecondAsterisk, true)
      .setPreference(RewriteArrowSymbols, false)
      .setPreference(SpacesAroundMultiImports, true)
      .setPreference(SpacesWithinPatternBinders, true)
  )

}

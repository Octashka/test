package example

import java.net.Socket
import java.io._
import akka.actor.{ ActorSystem, Props }

object Hello extends App {
  if (args.length == 0)
    println("restart with client settings " +
      "(s host:port file) or server (c host:port)")
  else if (args(0) == "c") {
    //Client
    if (args.length < 2) {
      println(s"Error: there are not enough arguments: passed ${args.length}, and need 2\n")
    } else {
      val b = args(1).split(":")
      println(b.length)
      if (b.length != 2) {
        println(s"Error: there are not enough arguments: ip or port ${b}\n")
      } else {
        val host = b(0)
        val port = b(1).toInt
        val socket = new Socket(host, port)
        val out = new BufferedOutputStream(socket.getOutputStream())
        val fileIn = new BufferedInputStream(new FileInputStream(args(2)))
        val buffer = new Array[Byte](fileIn.available())
        fileIn.read(buffer, 0, fileIn.available())
        out.write(buffer)
        println("Closing connection to " + host)
        out.close
      }
    }
  } else if (args(0) == "s") {
    //Server
    val b = args(1).split(":")
    val host = b(0)
    val port = b(1).toInt
    if (b.length != 2) {
      println(s"Error: there are not enough arguments: ip or port ${b}\n")
    } else {
      println(s"$host\t$port")
      val system = ActorSystem("ServerTest")
      system.actorOf(Props[TextProcessor], "TextProcessor")
      system.actorOf(Props(new Server(host, port)), name = "Server")
    }
  }
}

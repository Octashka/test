package example

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import akka.util.Timeout
import akka.pattern.ask
import scala.collection.immutable._
import scala.concurrent.Await
import scala.concurrent.duration._

object TextProcessor {
  final case class CalcSentence(idx: Int, Sentence: String)
  final case class Text(buffer: String)
}

class TextProcessor extends Actor with ActorLogging {
  log.info("TextProcessor Actor started")
  import TextProcessor._

  // function creating the actors of processing
  def fcall(idx: Int, sentence: String): Any = {
    val calc: ActorRef = context.actorOf(Props[CalcText])
    implicit val timeout: Timeout = Timeout(15.seconds)
    val future = calc ? CalcSentence(idx, sentence)
    val result = Await.result(future, timeout.duration)
    log.info(s"result: ${result}")
    result
  }

  def receive = {
    case Text(buffer: String) =>
      // selects sentences
      val reg = "([^\\.!?…]*[\\.!?…])".r
      val listSentences = reg.findAllIn(buffer).toList
      val str = listSentences.map(x => x.split("\n"))
      // removes unnecessary "jumps to a new line" in the sentence
      val sentence = str.map(_.mkString)
      val idx = List.range(1, sentence.length + 1)
      val s = idx.zip(sentence)
      // function that creates the processing agents
      val result = s.map(x => (x._1, fcall(x._1, x._2)))
      val s1 = result.unzip
      val s2 = (s1._1 zip s1._2).toMap
      val FinishWork = TreeMap(s2.toSeq: _*)
      log.info(s"$FinishWork")
  }
}

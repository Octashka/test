package example.parser

//"R       56700232017-04-28WBC00156.045mg/gl"
//    Чи-ой иден-ор  поряд №    Дата        последовательности    4х сим    значение,   6ти сим
//          матер.   матер.     YYYY-MM-DD      элементов           ИМЯ     9 символов   строка
//'R' '       567'   '0023'    '2017-04-28' 'WBC00156.045mg/gl' => 'WBC'    '00156.045' ' mg/gl'

import scala.util.parsing.combinator._

final case class Measurement(
  name: String,
  va: String,
  unit: String,
  flag: String
)

final case class Header(
  litteral: String,
  indMaterial: String,
  nMaterial: Option[String],
  gender: String,
  date: String
)

final case class Message(
  header: Header,
  measurement: List[Measurement]
)

trait MessageParser extends RegexParsers {
  override def skipWhitespace = false

  def indMaterial: Parser[String] = """[\s\d]{10}""".r ^^ {
    case i => i.trim
  }

  def nMaterial: Parser[String] = """\d{6}""".r

  def gender: Parser[String] = "[MmFfUu ]".r

  def date: Parser[String] = """(\d{4}-\d{2}-\d{2})""".r

  def name: Parser[String] = """[a-zA-Z]{2,4}""".r

  def signs: Parser[String] = ">=" | "<=" | ">" | "<"

  def number: Parser[String] = """\s*?[-]?\d+(\.?\d*)?""".r

  def v: Parser[String] = opt("""\s+""".r) ~> opt(signs) ~ number <~ opt("""\s+""".r) ^^ {
    case Some(s) ~ n => s + n
    case None ~ n => n
  }

  def unit: Parser[String] = """(\p{Alpha}+\/?\p{Alpha}+)""".r

  def flag: Parser[String] = "!" | " " | "*"

  def measurementList: Parser[List[Measurement]] = rep1(measurement) ^^ { a => a }

  def measurement: Parser[Measurement] = name ~ v ~ unit ~ flag ^^ {
    case n ~ v ~ u ~ f =>
      val suffix =
        if (v.contains("<") | v.contains(">")) "(qual)"
        else ""
      Measurement(n + suffix, v, u, f)
  }

  def header: Parser[Header] = "R" ~ indMaterial ~ opt(nMaterial) ~ gender ~ date ^^ {
    case "R" ~ iM ~ nM ~ g ~ d => Header("R", iM, nM, g, d)
  }

  def message: Parser[Message] = header ~ measurementList ^^ {
    case h ~ m => Message(h, m)
  }
}

package example

import java.net.InetSocketAddress

import akka.actor.{ Actor, ActorLogging, Props }
import akka.io.{ IO, Tcp }

class Server(host: String, port: Int) extends Actor with ActorLogging {
  import Tcp._
  import context.system
  IO(Tcp) ! Bind(self, new InetSocketAddress(host, port))

  def receive = {
    case b @ Bound(localAddress) =>
      log.info(s"Bound: ${localAddress}")

    case CommandFailed(_: Bind) =>
      log.info(s"Command failed: ${Bind}")
      context.stop(self)

    case c @ Connected(remote, local) =>
      log.info(s"Connection received from hostname: ${remote.getHostName} address: ${remote.getAddress.toString}")
      val connection = sender()
      val handler = system.actorOf(Props[Handler])
      connection ! Register(handler)
  }
}

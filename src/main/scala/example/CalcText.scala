package example

import scala.util.matching.Regex
import akka.actor.{ Actor, ActorLogging }

object CalcText {
  final case class SentenseStat(letters: Int, punc: Int, gaps: Int, words: Int)
}

class CalcText extends Actor with ActorLogging {
  import TextProcessor._
  import CalcText._

  def calculate(sentence: String, command: Regex) = command.findAllMatchIn(sentence).length

  def receive = {
    case CalcSentence(idx, sentence) =>
      log.info(s"${sentence}")
      val stat = SentenseStat(
        letters = calculate(sentence, "[a-zA-Zа-яА-Я]".r),
        gaps = calculate(sentence, "\\s+".r),
        punc = calculate(sentence, "[,_:_;_-_!?_+_\"]".r),
        words = calculate(sentence, "[a-zA-Zа-яА-Я]+".r)
      )
      log.info(s"$idx ${stat.toString}")
      sender ! stat
  }
}


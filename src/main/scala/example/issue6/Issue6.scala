package example.issue6
/*
'MODEL=A1\r'
'OperID=567\r'
'IDs=990123\r'
'SEQ=010102\r'
'ly%=0\r'
'sg%=0\r'
'eo%=0\r'
'mo%=0\r'
'ba%=0\r'
'st%=0\r'
'mm%=0\r'
'mi%=0\r'
'pm%=0\r'
'bl%=0\r'
'pl%=0\r'
MODEL: обязательное.
OperID: опциональное
IDs: обязательное
SEQ: опциональное.
Поля с результатами измерений имеют вид: <имя измерения> = <значение>
*/
final case class A1Message(
  model: String,
  operId: String,
  ids: String,
  seq: String,
  measurements: List[(String, String)]
)

import scala.util.parsing.combinator._
trait Message extends RegexParsers {
  override def skipWhitespace = false

  val modelConst = "MODEL"
  val operIdConst = "OperID"
  val idsConst = "IDs"
  val seqConst = "SEQ"

  val whitespace = opt(rep1(" "))

  def smooth: Parser[String] = whitespace ~> "=" <~ whitespace
  def eol: Parser[String] = whitespace ~> "\r" <~ whitespace
  def parser: Parser[String] = """\w+""".r <~ eol

  def line(name: String): Parser[String] = (name ~ smooth) ~> parser

  def model: Parser[String] = line(modelConst)

  def operId: Parser[String] = line(operIdConst)

  def ids: Parser[String] = line(idsConst)

  def seq: Parser[String] = line(seqConst)

  def namevalueMeas: Parser[(String, String)] = ("""\w+%""".r <~ smooth) ~ parser ^^ {
    case n ~ v => (n, v)
  }

  def measurements: Parser[List[(String, String)]] = rep(namevalueMeas)

  def message: Parser[A1Message] =
    model ~ opt(operId) ~ ids ~ opt(seq) ~ measurements ^^ {
      case m ~ o ~ i ~ s ~ meas =>
        A1Message(m, o.getOrElse(""), i, s.getOrElse(""), meas)
    }
}

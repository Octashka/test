package example

import akka.actor.{ Actor, ActorLogging, ActorSelection, ActorRef }
import akka.io.Tcp.{ PeerClosed, Received }
import akka.util.{ ByteString }

object Handler {
  final case class GetState(receiver: ActorRef)
}

class Handler extends Actor with ActorLogging {
  import Handler._
  import TextProcessor._
  var bufferByte = ByteString.empty
  var buffer = new String

  def receive = {
    case Received(data) =>
      //accumulates messages
      bufferByte ++= data

    case PeerClosed =>
      log.info("Peer closed")
      context.stop(self)
      buffer = bufferByte.decodeString("UTF-8")
      log.debug(s"Process: ${buffer}")
      val aSelection: ActorSelection =
        context.actorSelection("/user/TextProcessor")
      aSelection ! Text(buffer)

    case GetState(receiver) =>
      //specially for tests: Simulation PeerClosed
      log.info("GetState")
      buffer = bufferByte.decodeString("UTF-8")
  }
}


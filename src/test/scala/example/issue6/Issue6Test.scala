package example.issue6

import org.specs2.mutable
import org.specs2.matcher

class MessageParsers extends mutable.Specification
  with matcher.ParserMatchers {

  val parsers = new Message {}

  import parsers._

  "Simple parser" should {

    "Model parsers" in {
      "Message  MODEL = A1\\r:\n with whitespace, correct" >> {
        model must succeedOn("MODEL  =  A1\r").withResult("A1")
      }

      "Message  MODEL=A 1\\r:\n with whitespace, not correct" >> {
        model must failOn("MODEL = A 1\r")
      }

      "Model field with several '=' signs" >> {
        model must failOn("MODEL = = = = A1\r")
      }

    }

    "OperID parsers" in {
      "Message  OperID = 567\\r:\n with whitespace, correct" >> {
        operId must succeedOn("OperID = 567\r").withResult("567")
      }

      "Message  OperID=56 7\\r:\n with whitespace, not correct" >> {
        operId must failOn("OperID = 56 7\r")
      }
    }

    "IDs parsers" in {
      "Message  IDs= 990123\\r:\n with whitespace, correct" >> {
        ids must succeedOn("IDs= 990123\r").withResult("990123")
      }

      "Message  IDs=990 123\\r:\n with whitespace, not correct" >> {
        ids must failOn("IDs= 990 123\r")
      }
    }

    "SEQ parsers" in {
      "Message  SEQ= 010102\\r: \nwith whitespace   =  , correct" >> {
        seq must succeedOn("SEQ= 010102\r").withResult("010102")
      }

      "Message  SEQ=010102\\r: \n with whitespace, not correct" >> {
        seq must failOn("SEQ=010 102\r")
      }
    }

    "Measurement parsers" in {

      "there is could be no measurements at all" >> {
        measurements must succeedOn("").withResult(List[(String, String)]())
      }

      "discard empty or spaces lines" >> {
        measurements must failOn("pl%=1\r \r\r   \r")
      }

      "namevalueMeas of simple item, correct" >> {
        measurements must succeedOn("pl%= 0\r").withResult(
          List(("pl%", "0"))
        )
      }

      "namevalueMeas of two item, correct" >> {
        measurements must succeedOn("pl%= 0\rly%=21344\r").withResult(
          List(("pl%", "0"), ("ly%", "21344"))
        )
      }

      "namevalueMeas of two item, \nOne of the names or value with a space, not correct" >> {
        measurements must failOn("pl%= 0\rly %=213 44\r")
      }

      "namevalueMeas of two item, \nabsence of measurement name, not correct" >> {
        measurements must failOn("= 0\rly %=213 44\r")
      }

      "namevalueMeas of two item, \nthe value has alphabetic and numeric characters , correct" >> {
        measurements must succeedOn("pl%= A1\rly%=21344\r").withResult(
          List(("pl%", "A1"), ("ly%", "21344"))
        )
      }
    }
  }

  "Message Parser" should {

    "Message with empty lines in header" in {
      message must failOn("MODEL=A1\r  \r\rOperID=567\rIDs=990123\rSEQ=010102\rly%=0\r")
    }

    "Message full message: one measurement" in {
      message must succeedOn("MODEL=A1\rOperID=567\rIDs=990123\rSEQ=010102\rly%=0\r").withResult(
        A1Message(
          model = "A1",
          operId = "567",
          ids = "990123",
          seq = "010102",
          measurements = List(("ly%", "0"))
        )
      )
    }

    "Message incomlit operID or SEQ, correct" in {
      message must succeedOn("MODEL=A1\rIDs=990123\rly%=0\r").withResult(
        A1Message(
          model = "A1",
          operId = "",
          ids = "990123",
          seq = "",
          measurements = List(("ly%", "0"))
        )
      )
    }

        "Message full message,\n Spaces before and after the field names, correct" in {
      message must succeedOn("MODEL=A1 \rOperID=567\rIDs=990123\rSEQ=010102\rly% =0\r").withResult(
        A1Message(
          model = "A1",
          operId = "567",
          ids = "990123",
          seq = "010102",
          measurements = List(("ly%", "0"))
        )
      )
    }

      val measItem = "ly%=0\rsg%=0\reo%=0\rmo%=0\rba%=0\rst%=0\rmm%=0\rmi%=0\rpm%=0\rbl%=0\rpl%=0\r"
     "Message full many measurement, correct" in {
      message must succeedOn("MODEL=A1\rOperID=567\rIDs=990123\rSEQ=010102\r" + measItem).withResult(
        A1Message(
          model = "A1",
          operId = "567",
          ids = "990123",
          seq = "010102",
          measurements = List(
          ("ly%", "0"),
          ("sg%", "0"),
          ("eo%", "0"),
          ("mo%", "0"),
          ("ba%", "0"),
          ("st%", "0"),
          ("mm%", "0"),
          ("mi%", "0"),
          ("pm%", "0"),
          ("bl%", "0"),
          ("pl%", "0"),
          )
        )
      )
    }

    "Message incomlit measurement: value = empty, not correct" in {
      message must failOn("MODEL=A1\rIDs=990123\rly%=\r")
    }

    "Message full one measurement, fields are confused, not correct" in {
      message must failOn("OperID=567\rMODEL=A1\rIDs=990123\rSEQ=010102\rly%=0\r")
    }
  }
}

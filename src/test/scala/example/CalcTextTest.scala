package example

import akka.actor.{ ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit, TestActorRef }
import org.scalatest.{ BeforeAndAfterAll, WordSpecLike, MustMatchers }

class CalcTextTest_eng extends TestKit(ActorSystem("CalcTextTest_eng"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import TextProcessor._
  import CalcText._
  val verifying_text = "A simple sentence does not need anything"

  val idx = 1
  val qtywordT = 7
  val qtyletterT = 34 //???
  val qtypuncT = 0
  val qtygapsT = 6
  "CalcTextTest_eng Actor" must {
    "Send English text the proposal and check the counting in the sentence of words,  punctuation, spaces, letters" in {
      val calcText = TestActorRef[CalcText]
      calcText ! CalcSentence(idx, verifying_text)
      expectMsg(SentenseStat(qtyletterT, qtypuncT, qtygapsT, qtywordT))
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class CalcTextTest_rus extends TestKit(ActorSystem("CalcTextTest_rus"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import TextProcessor._
  import CalcText._
  val verifying_text = "Простому предложению ничего не нужно"
  val idx = 1
  val qtywordT = 5
  val qtyletterT = 32 //???
  val qtypuncT = 0
  val qtygapsT = 4
  "CalcTextTest_rus Actor" must {
    "Send English text the proposal and check the counting in the sentence of words,  punctuation, spaces, letters" in {
      val calcText = TestActorRef[CalcText]
      calcText ! CalcSentence(idx, verifying_text)
      expectMsg(SentenseStat(qtyletterT, qtypuncT, qtygapsT, qtywordT))
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class CalcTextTest_rus_withPuncAndSpac extends TestKit(ActorSystem("CalcTextTest_rus_withPuncAndSpac"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import TextProcessor._
  import CalcText._
  val verifying_text = "И он сказал, подумав немного,- \"Раздам ли всем я по ушам?!\""
  val idx = 1
  val qtywordT = 11
  val qtyletterT = 42 //??
  val qtypuncT = 6
  val qtygapsT = 10
  "CalcTextTest_rus Actor" must {
    "Send Rus text with punctuation marks, with spaces the proposal and check the counting in the sentence of words,  punctuation, spaces, letters" in {
      val calcText = TestActorRef[CalcText]
      calcText ! CalcSentence(idx, verifying_text)
      expectMsg(SentenseStat(qtyletterT, qtypuncT, qtygapsT, qtywordT))
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class CalcTextTest_rus_withPuncNoSpac extends TestKit(ActorSystem("CalcTextTest_rus_withPuncNoSpac"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import TextProcessor._
  import CalcText._
  val verifying_text = "Примерыпричастий:покупающаясладостидевочка,составленныйучителемсписок,смотрящийпосторонампешеход,спешащийнаработумужчина"
  val idx = 1
  val qtywordT = 5
  val qtyletterT = 116 //???
  val qtypuncT = 4
  val qtygapsT = 0
  "CalcTextTest_rus_withPuncNoSpac Actor" must {
    "Send Rus text with punctuation marks, no spaces the proposal and check the counting in the sentence of words,  punctuation, spaces, letters" in {
      val calcText = TestActorRef[CalcText]
      calcText ! CalcSentence(idx, verifying_text)
      expectMsg(SentenseStat(qtyletterT, qtypuncT, qtygapsT, qtywordT))
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class CalcTextTest_rus_noPuncNoSpac extends TestKit(ActorSystem("CalcTextTest_rus_withPuncNoSpac"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import TextProcessor._
  import CalcText._
  val verifying_text = "Вместекласснотекстписатьбезпробелов"
  val idx = 1
  val qtywordT = 1
  val qtyletterT = 35 //????
  val qtypuncT = 0
  val qtygapsT = 0
  "CalcTextTest_rus_noPuncNoSpac Actor" must {
    "Send Rus text noPuncNoSpac the proposal and check the counting in the sentence of words,  punctuation, spaces, letters" in {
      val calcText = TestActorRef[CalcText]
      calcText ! CalcSentence(idx, verifying_text)
      expectMsg(SentenseStat(qtyletterT, qtypuncT, qtygapsT, qtywordT))
    }
  }
  override def afterAll(): Unit = shutdown(system)
}


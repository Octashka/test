package example.parser

import org.specs2.mutable
import org.specs2.matcher

// TODO Тесты должны проверять как успешный разбор допустимых значений,
// так и отказ разбора недопустимых значнеий.
class A1ParsersSpec extends mutable.Specification
  with matcher.ParserMatchers {

  val parsers = new MessageParser {}

  import parsers._

  "Header fields' parsers" should {

    "Field `indMaterial`" >> {

      "length < 10, not correct" >> {
        indMaterial must failOn("567")
      }

      "the value is not left-aligned, correct " >> {
        indMaterial must succeedOn("  567     ").withResult("567")
      }

      "can't contain letters" >> {
        indMaterial must failOn("  567   dd")
        indMaterial must failOn("      g567")
      }

      "source string greater than parser, not correct " >> {
        indMaterial must failOn("       567002222")
      }

    }

    "Field `nMaterial`" >> {

      "length > 6, not correct" >> {
        nMaterial must failOn("000023asda")
      }

      "length < 6, not correct" >> {
        nMaterial must failOn("00023")
      }

      "can't contain letters" >> {
        nMaterial must failOn("00s02")
        nMaterial must failOn("s0002")
        nMaterial must failOn("0002s")
      }

    }

    "Field `Gender`" >> {

      "accept 'female'" >> {
        gender must succeedOn("f").withResult("f")

        gender must succeedOn("F").withResult("F")
      }

      "accept male" >> {

        gender must succeedOn("m").withResult("m")

        gender must succeedOn("M").withResult("M")

      }

      "accept unknown gender" >> {
        gender must succeedOn(" ").withResult(" ")

        gender must succeedOn("u").withResult("u")

        gender must succeedOn("U").withResult("U")
      }

      "discard digits" >> {
        gender must failOn("2")
      }

      "discard unallowed symbols" >> {
        gender must failOn("a")
        gender must failOn("W")
      }

      "discard digits from next date field" >> {
        gender must succeedOn("f20012").partially.withResult("f")
      }

    }

    "Field `Date`" >> {

      "format 'YYYY-MM-DD', correct" >> {
        date must succeedOn("2017-04-28")
      }

      "format 'YY-MM-DD' incorrect" >> {
        date must failOn("17-04-28")
      }

      "discard date without dashes" >> {
        date must failOn("20170428")
      }

      "ignore letters from measurement name" >> {
        date must succeedOn("2017-10-12WBC").partially.withResult("2017-10-12")
      }

    }

  }

  "Parser of the entire header" should {

    "Accept header with all available fields" >> {
      header must succeedOn("R       567000023f2017-04-28").
        withResult(
          Header(
            litteral = "R",
            indMaterial = "567",
            nMaterial = Some("000023"),
            gender = "f",
            date = "2017-04-28"
          )
        )
    }

    "Discard header with unknown message type" >> {
      header must failOn("G       567000023f2017-04-28")
    }

    "Discard header with incomplete `indMaterial`" >> {
      header must failOn("R     5670000232017-04-28")
    }

    "Accept header without optional `nMaterial` field" >> {
      header must succeedOn("R       567f2017-04-28").
        withResult(
          Header(
            litteral = "R",
            indMaterial = "567",
            nMaterial = None,
            gender = "f",
            date = "2017-04-28"
          )
        )
    }

    "Discard header with reversed by two fields " >> {
      header must failOn("R       567f0000232017-04-28")
    }

    "Discard header with incomplete date field" >> {
      header must failOn("R     5670000f232017")
    }

  }

  "Measurements parser" should {

    "Measurement list parser" >> {

      "discard empty list" >> {
        measurementList must failOn("")
      }

      "accept single item" >> {
        measurementList must succeedOn("RWBC <= -156.045g/L!").
          withResult(
            List(
              Measurement(
                name = "RWBC(qual)",
                va = "<= -156.045",
                unit = "g/L",
                flag = "!"
              )
            )
          )
      }

      "accept two items" >> {
        measurementList must succeedOn("CV156.045g/L!pH200.045mmol/l*").
          withResult(
            List(
              Measurement(
                name = "CV",
                va = "156.045",
                unit = "g/L",
                flag = "!"
              ),
              Measurement(
                name = "pH",
                va = "200.045",
                unit = "mmol/l",
                flag = "*"
              )
            )
          )
      }

      "discard incomplete single item" >> {

        "missing flag field" >> {
          measurementList must failOn("CV156.045g/L")
        }

        "missing units and flag" >> {
          measurementList must failOn("CV156.045")
        }

        "missing all expcept name" >> {
          measurementList must failOn("CV")
        }

      }

      "discard single item and incomplete second item (has only name)" >> {
        measurementList must failOn("CV156.045g/L!WBC")
      }

    }

    "Measurement fields' parsers" >> {

      "`name` parser" >> {

        "with longer than necessary, correct" >> {
          name must succeedOn("RWBCpH").partially.withResult("RWBC")
        }

        "with a digit( or punctuation marks), not correct" >> {
          name must failOn("RW,BCpH")
        }

        "name single-letter , not correct" >> {
          name must failOn("R")
        }
      }

      "`value` parser" >> {
        "number: bare integer" >> {
          v must succeedOn("234").withResult("234")
        }

        "number float" >> {
          v must succeedOn(" 2.2").withResult("2.2")
        }

        "number with arithmetic. sign." >> {
          v must succeedOn("-22.2").withResult("-22.2")
        }

        "number with a sign of comparison." >> {
          v must succeedOn(" >= 22. ").withResult(">= 22.")
        }

        "number with arithmetic. sign in the middle ('3-4.0')" >> {
          v must failOn("3-4.0")
        }

        "number with a sign of comparison in the middle" >> {
          v must failOn("3>4.0")
        }

        "arithmetic. sign, comparison sign, number ('+> = 140.4')" >> {
          v must failOn("+> = 140.4")
        }

      }

      "`unit` parser" >> {

        "accept single word" >> {
          unit must succeedOn("mmol")
        }

        "accept words separated with slash" >> {
          unit must succeedOn("mmol/L")
        }

        "discard slash only" >> {
          unit must failOn("/")
        }

        "discard value that stars or ends with slash" >> {
          unit must failOn("/mmol")
          unit must failOn("mmol/")
        }

        "doesn't consume flag from source string after units" >> {
          unit must succeedOn("g/L!").partially.withResult("g/L")
        }

        "string with digits incorrect" >> {
          unit must failOn("g22/L")
        }

      }

      "`flag` parser" >> {

        "accept allowed values" >> {
          flag must succeedOn("!").withResult("!")
          flag must succeedOn(" ").withResult(" ")
          flag must succeedOn("*").withResult(equalTo("*"))
        }

        "discard unallowed symbols" >> {
          flag must failOn("1")
          flag must failOn("w")
          flag must failOn("-")
        }

        "flag with longer than necessary" >> {
          flag must succeedOn("!asd").partially.withResult("!")
        }
      }
    }

    "Entire message parser" should {

      val msgFuLL = "R       567000023f2017-04-28RWBC -156.045g/L*"

      "Discard empty string (message unavailable)" in {
        message must failOn("")
      }

      "Accept message with single measurement" in {
        message must succeedOn(msgFuLL).
          withResult(
            Message(
              Header(
                litteral = "R",
                indMaterial = "567",
                nMaterial = Some("000023"),
                gender = "f",
                date = "2017-04-28"
              ),
              List(
                Measurement(
                  name = "RWBC",
                  va = "-156.045",
                  unit = "g/L",
                  flag = "*"
                )
              )
            )
          )
      }

      "Discard message without results" in {
        message must failOn("R       567000023f2017-04-28")
      }

    }
  }
}


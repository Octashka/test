package example

import akka.actor.{ ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit, TestActorRef }
import org.scalatest.{ BeforeAndAfterAll, WordSpecLike, MustMatchers }
import akka.io.Tcp.{ Received }
import akka.util.ByteString

class HandlerTestFirst extends TestKit(ActorSystem("HandlerTestFirst"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import Handler._
  val msg01 = "some, "
  val msg02 = "buffer."
  "Handler Actor First" must {
    "append buffer = 'some, ' + 'buffer'" in {
      val handler = TestActorRef[Handler]
      handler ! Received(ByteString(msg01))
      handler ! Received(ByteString(msg02))
      handler ! GetState(testActor)
      handler.underlyingActor.buffer must equal(msg01 + msg02)
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class HandlerTestSecond extends TestKit(ActorSystem("HandlerTestSecond"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import Handler._
  val msg02 = "П"
  val msgbyte = ByteString(msg02)
  "Handler Actor Second" must {
    "append buffer 'П' two bytes per byte" in {
      val handler = TestActorRef[Handler]
      handler ! Received(ByteString(msgbyte(0)))
      handler ! Received(ByteString(msgbyte(1)))
      handler ! GetState(testActor)
      handler.underlyingActor.buffer must equal(msg02)
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

class HandlerTestThird extends TestKit(ActorSystem("HandlerTestThird"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {
  import Handler._
  val msg02 = "Р"
  val msgbyte = ByteString(msg02)
  var errorstring = new String
  "Handler Actor Third" must {
    "append buffer 'Р' sends one byte" in {
      val handler = TestActorRef[Handler]
      handler ! Received(ByteString(msgbyte(0)))
      handler ! GetState(testActor)
      errorstring = ByteString(msgbyte(0)).decodeString("Utf-8")
      handler.underlyingActor.buffer must equal(errorstring)
    }
  }
  override def afterAll(): Unit = shutdown(system)
}

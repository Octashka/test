lazy val akkaVersion = "2.5.12"
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "bolshakov",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.scalatest" %% "scalatest" % "3.0.1" % "test",
      "org.specs2" %% "specs2-core" % "4.2.0" % Test,
      "org.specs2" %% "specs2-matcher-extra" % "4.2.0" % Test,
      "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.5"
    )
  )
  .settings(Formatting.settings: _*)
  .settings(
    scapegoatVersion in ThisBuild := "1.3.5",
    wartremoverWarnings in (Compile, compile) ++= Seq(),
    wartremoverErrors in (Compile, compile) ++= Seq(
      Wart.ArrayEquals,
      Wart.EitherProjectionPartial,
      Wart.ExplicitImplicitTypes,
      Wart.FinalCaseClass,
      Wart.FinalVal,
      Wart.JavaConversions,
      Wart.Null,
      Wart.OptionPartial,
      Wart.Return,
      Wart.StringPlusAny,
      Wart.TraversableOps,
      Wart.TryPartial
    ),
    scalacOptions in Test ++= Seq("-Yrangepos"),
    scalacOptions ++= Seq(
      "-deprecation",
      "-feature",
      "-language:implicitConversions",
      "-language:existentials",
      "-language:higherKinds",
      // Exclude `unused`: desired combination assigned by `-Ywarn-unused:...`
      "-Xlint:-unused,_",
      "-Xcheckinit",
      "-Xfuture",
      "-Ycache-plugin-class-loader:last-modified",
      "-Ycache-macro-class-loader:last-modified",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-extra-implicit",
      "-Ywarn-inaccessible",
      "-Ywarn-nullary-unit",
      "-Ywarn-nullary-override",
      "-Ywarn-value-discard",
      // @note 2.12 only. excluded: `params`, patvars
      // for 2.11 use "-Ywarn-unused" and "-Ywarn-unused-import"
      "-Ywarn-unused:imports,privates,locals,implicits",
      "-Ywarn-dead-code",
      "-Yinfer-argument-types",
      "-explaintypes",
      "-unchecked",
      "-g:vars",
      "-encoding", "UTF8"
    ),
    scalacOptions in (Compile, console) ~= (_ filterNot (_ == "-Ywarn-unused:imports,privates,locals,implicits"))
  )
